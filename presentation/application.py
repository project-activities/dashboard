from dash import Dash
import dash_bootstrap_components as bootstrap

from presentation.views.index import index


def create_app():
    app = Dash(
        __name__,
        suppress_callback_exceptions=True,
        external_stylesheets=[bootstrap.themes.BOOTSTRAP],
    )
    app.layout = index

    return app
