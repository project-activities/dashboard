from dash import html, callback, Input, Output, dcc
import dash_bootstrap_components as bootstrap

from presentation.views.example import example


SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "18rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

index = html.Div(
    [
        html.Div(id='sidebar', children=
            [
                html.H2('Categories', className='display-4'),
                 html.P('some text about categories'),
                html.Hr(),

                dcc.Location(id='url', refresh=False),

                bootstrap.Nav(
                    [
                        bootstrap.NavLink('Category 1', href='/category_1', active='exact'),
                        bootstrap.NavLink('Category 2', href='/category_2', active='exact'),
                        bootstrap.NavLink('Category 3', href='/category_3', disabled=True),
                    ],
                    vertical=True,
                    pills=True,
                ),
            ],
            style=SIDEBAR_STYLE
        ),

        html.Div(id='page-content', style=CONTENT_STYLE),
    ],
)


@callback(
    Output(component_id='page-content', component_property='children'),
    Input(component_id='url', component_property='pathname')
)
def display_page(pathname):
    if pathname == '/category_1':
        return example
    if pathname == '/category_2':
        return pathname