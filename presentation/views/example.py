from dash import html
import dash_pivottable as table
import csv


"""Временно"""
with open('entities.csv', encoding='utf8') as file:
    reader = csv.reader(file)
    data = [row for row in reader]

example = html.Div(
    [
        table.PivotTable(
            data=data
        )
    ]
)